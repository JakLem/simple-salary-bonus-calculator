<?php

namespace App;

use App\CalculateStrategy\MultiplyCalculation;
use App\CalculateStrategy\PercentageCalculation;
use Money\Money;
use NumberLib\Number;

class ExtraBonusCalculator
{
    public function __construct(
        private ExtraBonusComponentsDto $bonusComponentsDto,
        int $seniorityLimit = 10
    ) {
        $this->bonusComponentsDto->seniority =
            $this->bonusComponentsDto->seniority
                ->not(fn ($numberValue) => min($numberValue, $seniorityLimit));
    }

    public function salary(): Salary
    {
        $salaryValueInPenny = $this->bonusComponentsDto->salary->multiply(Number::of(100));
        $bonus = $this->bonus();
        $salary = Money::PLN($salaryValueInPenny->int());

        return Salary::ofWithBonus($salary, $bonus);
    }

    public function bonus(): ExtraBonus
    {
        $percentBonus = (new PercentageCalculation())
            ->calculate($this->bonusComponentsDto->salary, $this->bonusComponentsDto->percentBonus);
        $constBonus = (new MultiplyCalculation())
            ->calculate($this->bonusComponentsDto->seniority, $this->bonusComponentsDto->constBonus);

        return new ExtraBonus(
            $percentBonus,
            $constBonus
        );
    }
}