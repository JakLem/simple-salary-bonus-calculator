<?php

namespace App\CalculateStrategy;

use NumberLib\Number;

class PercentageCalculation implements Calculation
{
    public function calculate(Number $a, Number $b): Number
    {
        return $a->percent($b);
    }
}