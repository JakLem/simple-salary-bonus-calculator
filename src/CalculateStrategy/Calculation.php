<?php

namespace App\CalculateStrategy;

use NumberLib\Number;

interface Calculation
{
    public function calculate(Number $a, Number $b): Number;
}