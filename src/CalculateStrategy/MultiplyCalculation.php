<?php

namespace App\CalculateStrategy;

use NumberLib\Number;

class MultiplyCalculation implements Calculation
{
    public function calculate(Number $a, Number $b): Number
    {
        return $a->multiply($b);
    }
}