<?php

namespace App;

use App\ORMEntities\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

interface EmployeeRepository
{
    public function find(UuidInterface $employeeId): Employee;
    public function findAll(): ArrayCollection;
}