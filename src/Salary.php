<?php

namespace App;

use JetBrains\PhpStorm\Pure;
use Money\Money;

class Salary
{
    private function __construct(
        private Money      $value,
        private ExtraBonus $extraBonus,
    ) {
    }

    #[Pure] public static function ofWithBonus(Money $value, ExtraBonus $extraBonus): self
    {
        $value = $value->add($extraBonus->bonus());
        return new self($value, $extraBonus);
    }

    public function constBonus(): Money
    {
        return $this->extraBonus->constBonus();
    }

    public function percentageBonus(): Money
    {
        return $this->extraBonus->percentBonus();
    }

    public function withoutBonus(): Money
    {
        return $this->value->subtract($this->extraBonus->bonus());
    }

    public function withBonus(): Money
    {
        return $this->value;
    }
}