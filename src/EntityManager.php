<?php

namespace App;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Tools\Setup;

class EntityManager
{
    private static array $instances = [];

    private function __construct()
    {
    }

    private function __clone(): void
    {
    }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance()
    {
        $thisClassName = static::class;
        if (!isset(self::$instances[$thisClassName])) {
            self::$instances[$thisClassName] = self::buildEntityManager();
        }

        return self::$instances[$thisClassName];
    }

    private static function buildEntityManager(): \Doctrine\ORM\EntityManager
    {
        // Create a simple "default" Doctrine ORM configuration for Annotations
        $isDevMode = true;
        $proxyDir = null;
        $cache = null;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAttributeMetadataConfiguration(
            array(__DIR__ . ""),
            $isDevMode, $proxyDir, $cache
        );
        // or if you prefer yaml or XML
        // $config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);
        // $config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);

        // database configuration parameters
        $conn = array(
            'driver' => 'pdo_sqlite',
            'path' => __DIR__ . '/../config/db.sqlite',
        );

        if (!Type::hasType('uuid')) {
            Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');
        }

        if (!Type::hasType('money')) {
            Type::addType('money', MyMoney::class);

        }

        return \Doctrine\ORM\EntityManager::create($conn, $config);
    }
}