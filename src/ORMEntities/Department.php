<?php

namespace App\ORMEntities;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

#[Entity]
#[Table(name: "departments")]
class Department
{
    #[Id]
    #[Column(type: "uuid", unique: true)]
    #[GeneratedValue(strategy: "CUSTOM")]
    #[CustomIdGenerator(class: UuidGenerator::class)]
    private UuidInterface $id;

    #[Column(type: "text")]
    private string $name;

    #[Column(type: "integer")]
    private int $percentBonusValue;

    #[Column(type: "integer")]
    private int $constBonusValue;

    public function __construct(
        string $name,
        int $percentBonusValue,
        int $constBonusValue
    ) {
        $this->name = $name;
        $this->percentBonusValue = $percentBonusValue;
        $this->constBonusValue = $constBonusValue;
    }

    public function getPercentValue(): int
    {
        return $this->percentBonusValue;
    }

    public function getConstValue(): int
    {
        return $this->constBonusValue;
    }
}