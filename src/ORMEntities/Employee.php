<?php

namespace App\ORMEntities;

use App\ExtraBonusCalculator;
use App\ExtraBonusComponentsDto;
use App\Salary;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\Embedded;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Money\Money;
use NumberLib\Number;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

#[Entity]
#[Table(name: "employees")]
class Employee
{
    #[Id]
    #[Column(type: "uuid", unique: true)]
    #[GeneratedValue(strategy: "CUSTOM")]
    #[CustomIdGenerator(class: UuidGenerator::class)]
    private UuidInterface $id;

    #[Column(type: "text")]
    private string $name;

    #[Column(type: "text")]
    private string $surname;

    #[ManyToOne(targetEntity: Department::class)]
    private Department $department;

    #[Column(type: "integer")]
    private int $seniority;

    #[Column(type: "money")]
    #[Embedded(class: Money::class)]
    private Money $salary;
    
    public function __construct(
        string $name, string $surname, int $seniority, Money $salary, Department $department
    ) {
        $this->name = $name;
        $this->surname = $surname;
        $this->seniority = $seniority;
        $this->salary = $salary;
        $this->department = $department;
    }

    public function extraBonusComponents(): ExtraBonusComponentsDto
    {
        return new ExtraBonusComponentsDto(
            Number::of($this->salary->getAmount()),
            Number::of($this->seniority),
            Number::of($this->department->getPercentValue()),
            Number::of($this->department->getConstValue()),
        );
    }

    public function calculateBonusSalary(ExtraBonusCalculator $calculator): Salary
    {
        return $calculator->salary();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return UuidInterface
     */
    public function getDepartment(): UuidInterface
    {
        return $this->department;
    }

    /**
     * @return string
     */
    public function getSeniority(): string
    {
        return $this->seniority;
    }

    /**
     * @return string
     */
    public function getSalary(): string
    {
        return $this->salary;
    }
}