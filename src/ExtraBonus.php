<?php

namespace App;

use Money\Money;
use NumberLib\Number;

class ExtraBonus
{
    private Number $salaryPercentageBonus;
    private Number $salaryConstBonus;

    public function __construct(
        Number $percentageBonus,
        Number $constBonus,
    ) {
        $this->salaryPercentageBonus = $percentageBonus;
        $this->salaryConstBonus = $constBonus;
    }

    public function percentBonus(): Money
    {
        return Money::PLN($this->salaryPercentageBonus->int() * 100);
    }

    public function constBonus(): Money
    {
        return Money::PLN($this->salaryConstBonus->int() * 100);
    }

    public function bonus(): Money
    {
        return $this->constBonus()->add($this->percentBonus());
    }
}
