<?php

namespace App\Command;

use App\EmployeeSalaryExtraSimulateService;
use App\ExtraBonusComponentsDto;
use NumberLib\Number;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TakeSalaryReportSimulateCommand extends Command
{
    protected static $defaultName = 'app:salary-report:simulate';

    protected function configure()
    {
        $this->addArgument('salary', InputArgument::OPTIONAL);
        $this->addArgument('seniority', InputArgument::OPTIONAL);
        $this->addArgument('percentBonus', InputArgument::OPTIONAL);
        $this->addArgument('constBonus', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Salary Report");

        $salary = $input->getArgument('salary') ?? 0;
        $seniority = $input->getArgument('seniority') ?? 0;
        $percentBonus = $input->getArgument('percentBonus') ?? 0;
        $constBonus = $input->getArgument('constBonus') ?? 0;

        list($salary, $seniority, $percentBonus, $constBonus) = Number::ofArray([
            $salary,
            $seniority,
            $percentBonus,
            $constBonus
        ]);

        $components = new ExtraBonusComponentsDto($salary, $seniority, $percentBonus, $constBonus);
        $result = (new EmployeeSalaryExtraSimulateService())->calculateFor($components);

        $output->writeln("Only Salary: " . $result->withoutBonus()->getAmount() / 100);
        $output->writeln("Salary With Bonus: " . $result->withBonus()->getAmount() / 100);
        $output->writeln("Const Bonus: " . $result->constBonus()->getAmount() / 100);
        $output->writeln("Percent Bonus: " . $result->percentageBonus()->getAmount() / 100);

        return Command::SUCCESS;
    }
}