<?php

namespace App\Command;

use App\EmployeeRepositoryImpl;
use App\EmployeeSalaryExtraSimulateService;
use App\EntityManager;
use App\ORMEntities\Employee;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetSalaryReportCommand extends Command
{
    protected static $defaultName = 'app:salary-report';

    protected function configure()
    {
        $this->addArgument('names', InputArgument::IS_ARRAY| InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filters = ['names' => $input->getArgument('names')];
        $repository = new EmployeeRepositoryImpl(EntityManager::getInstance());
        $employees = $repository->findAll();
        $employees = $employees->filter(function (Employee $employee) use ($filters) {
            if (!empty($filters['names'])) {
                return in_array($employee->getName(), $filters['names']);
            }
            return $employee;
        });

        foreach ($employees as $employee) {
            $output->writeln("Salary Report for " . $employee->getName() . " " . $employee->getSurname());
            $result = (new EmployeeSalaryExtraSimulateService())->calculateFor($employee->extraBonusComponents());

            $output->writeln("Only Salary: " . $result->withoutBonus()->getAmount() / 100);
            $output->writeln("Salary With Bonus: " . $result->withBonus()->getAmount() / 100);
            $output->writeln("Const Bonus: " . $result->constBonus()->getAmount() / 100);
            $output->writeln("Percent Bonus: " . $result->percentageBonus()->getAmount() / 100);
            $output->writeln("");
        }


        return Command::SUCCESS;
    }
}