<?php

namespace App;

class EmployeeSalaryExtraSimulateService
{
    public function calculateFor(ExtraBonusComponentsDto $components): Salary
    {
        $calculator = new ExtraBonusCalculator($components);

        return $calculator->salary();
    }
}