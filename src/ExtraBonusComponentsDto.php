<?php

namespace App;

use NumberLib\Number;

class ExtraBonusComponentsDto
{
    public function __construct(
        public Number $salary,
        public Number $seniority,
        public Number $percentBonus,
        public Number $constBonus,
    ) {
    }
}