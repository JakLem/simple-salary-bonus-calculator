<?php

namespace App;

use Ramsey\Uuid\UuidInterface;

class EmployeeSalaryExtraCalculateService
{
    public function __construct(
        private EmployeeRepository $resourceRepository
    ) {
    }

    public function calculateFor(UuidInterface $employeeId): Salary
    {
        $employee = $this->resourceRepository->find($employeeId);
        return $employee->calculateBonusSalary(
            new ExtraBonusCalculator($employee->extraBonusComponents())
        );
    }
}