<?php

namespace App;

use App\ORMEntities\Employee;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\UuidInterface;

class EmployeeRepositoryImpl implements EmployeeRepository
{
    private EntityRepository $repository;

    public function __construct(private \Doctrine\ORM\EntityManager $entityManager)
    {
        $this->repository = $this->entityManager->getRepository(Employee::class);
    }

    public function find(UuidInterface $employeeId): Employee
    {
        /**
         * @var Employee $employee
         */
        return $this->repository->find($employeeId);
    }

    public function findAll(): ArrayCollection
    {
        return  new ArrayCollection($this->repository->findAll());
    }

}