<?php

namespace Tests;

use App\CalculateStrategy\MultiplyCalculation;
use App\CalculateStrategy\PercentageCalculation;
use NumberLib\Number;
use PHPUnit\Framework\TestCase;

class CalculateStrategyTest extends TestCase
{
    public function testShouldCalculatePercentWithStrategy()
    {
        //given
        $expected = Number::of(150);
        $a = Number::of(1000);
        $percent = Number::of(15);


        $calculation = new PercentageCalculation();

        //when
        $result = $calculation->calculate($a, $percent);

        //then
        self::assertEquals($expected, $result);
    }
    public function testShouldCalculateConstWithStrategy()
    {
        //given
        $expected = Number::of(1000);
        $const = Number::of(100);
        $multiplier = Number::of(10);


        $calculation = new MultiplyCalculation();

        //when
        $result = $calculation->calculate($multiplier, $const);

        //then
        self::assertEquals($expected, $result);
    }
}