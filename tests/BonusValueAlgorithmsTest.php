<?php

namespace Tests;

use App\ExtraBonusCalculator;
use App\ExtraBonusComponentsDto;
use Money\Money;
use NumberLib\Number;
use PHPUnit\Framework\TestCase;

class BonusValueAlgorithmsTest extends TestCase
{
    public function testShouldCalculateProperlyPercentageBonusValue()
    {
        //given
        $expectedBonusValue = Money::PLN(15000);

        $salary =  Number::of(1000);
        $seniority =  Number::of(5);
        $percentBonus =  Number::of(15);
        $constBonus =  Number::of(0);

        $components = new ExtraBonusComponentsDto(
            $salary, $seniority, $percentBonus, $constBonus
        );

        $employee = new ExtraBonusCalculator($components);
        //when
        $result = $employee->bonus();

        //then
        self::assertEquals($result->percentBonus(), $expectedBonusValue);
    }

    public function testShouldCalculateProperlyForConstBonusValue()
    {
        //given
        $expectedBonusValue = Money::PLN(100000);

        $salary =  Number::of(0);
        $seniority =  Number::of(55);
        $percentBonus =  Number::of(0);
        $constBonus =  Number::of(100);

        $components = new ExtraBonusComponentsDto(
            $salary, $seniority, $percentBonus, $constBonus
        );
        $employee = new ExtraBonusCalculator($components);
        //when
        $result = $employee->bonus();

        //then
        self::assertEquals($result->constBonus(), $expectedBonusValue);
    }

    public function testShouldCalculateProperlyForAllBonusValue()
    {
        //given
        $expectedBonusValue = Money::PLN(81000);

        $salary =  Number::of(1000);
        $seniority =  Number::of(7);
        $percentBonus =  Number::of(11);
        $constBonus =  Number::of(100);

        $components = new ExtraBonusComponentsDto(
            $salary, $seniority, $percentBonus, $constBonus
        );
        $employee = new ExtraBonusCalculator($components);
        //when
        $result = $employee->bonus();

        //then
        self::assertEquals($result->bonus(), $expectedBonusValue);
    }

    public function testShouldCalculateProperlyForConstBonusValueForNotDefaultLimit()
    {
        //given
        $expectedBonusValue = Money::PLN(150000);

        $salary =  Number::of(0);
        $seniority =  Number::of(17);
        $percentBonus =  Number::of(0);
        $constBonus =  Number::of(100);

        $components = new ExtraBonusComponentsDto(
            $salary, $seniority, $percentBonus, $constBonus
        );
        $employee = new ExtraBonusCalculator($components, 15);
        //when
        $result = $employee->bonus();

        //then
        self::assertEquals($result->constBonus(), $expectedBonusValue);
    }
}