<?php

namespace Tests;

use App\ExtraBonus;
use App\Salary;
use Money\Money;
use NumberLib\Number;
use PHPUnit\Framework\TestCase;

class SalaryTest extends TestCase
{
    public function testShouldCalculateCompleteSalaryCalculate()
    {
        $expectedWith = Money::PLN(115000);
        $expectedWithout = Money::PLN(100000);

        $extraBonus = new ExtraBonus(
            Number::of(100),
            Number::of(50),
        );

        $salaryWithBonus = Salary::ofWithBonus(Money::PLN(100000), $extraBonus);

        self::assertEquals($expectedWith, $salaryWithBonus->withBonus());
        self::assertEquals($expectedWithout, $salaryWithBonus->withoutBonus());
    }
}