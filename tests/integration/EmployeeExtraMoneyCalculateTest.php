<?php

namespace Tests\integration;

use App\ExtraBonus;
use App\ExtraBonusCalculator;
use App\ExtraBonusComponentsDto;
use App\EmployeeRepository;
use App\ORMEntities\Department;
use App\ORMEntities\Employee;
use App\Salary;
use App\EmployeeSalaryExtraCalculateService;
use Money\Money;
use NumberLib\Number;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class EmployeeExtraMoneyCalculateTest extends TestCase
{
    private EmployeeRepository|MockObject $repository;

    protected function setUp(): void
    {
        $this->repository = $this->buildRepository();
    }

    public function testShouldCalculateSalaryWithBonus()
    {
        //given
        $expected = Salary::ofWithBonus(
            Money::PLN(100000),
            new ExtraBonus(Number::of(200), Number::of(250))
        );
        $service = new EmployeeSalaryExtraCalculateService($this->repository);

        //when
        $salary = $service->calculateFor(Uuid::uuid4());

        //then
        self::assertEquals($expected, $salary);
    }
    
    private function testShouldCalculateExtraMoneyForEmployee()
    {
        //given
        $calculator = $this->repository->find(Uuid::uuid4());
        $bonus = $calculator->bonus();

        //when
        $percentBonus = $bonus->percentBonus();
        $constBonus = $bonus->constBonus();
        $bonus = $bonus->bonus();

        //then
        $percentAssertResult = $percentBonus->equals(Money::PLN(20000));
        $constAssertResult = $constBonus->equals(Money::PLN(25000));
        $bonusAssertResult = $bonus->equals(Money::PLN(45000));

        self::assertTrue($percentAssertResult && $constAssertResult && $bonusAssertResult);
    }

    private function buildRepository(): MockObject|EmployeeRepository
    {
        $repository = $this->createMock(EmployeeRepository::class);
        $repository->method('find')->willReturn($this->buildEmployee());

        return $repository;
    }

    private function buildEmployee(): Employee
    {
        $department = new Department("", 20, 50);
        return new Employee("", "", 5, Money::PLN(1000), $department);
    }
}