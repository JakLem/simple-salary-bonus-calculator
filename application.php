<?php

require __DIR__.'/vendor/autoload.php';

use App\Command\GetSalaryReportCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new GetSalaryReportCommand());

$application->run();