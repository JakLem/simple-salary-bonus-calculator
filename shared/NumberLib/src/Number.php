<?php

namespace NumberLib;

use JetBrains\PhpStorm\Pure;

class Number
{
    private function __construct(
        private float $value,
        private NumberState $state
    ) {
    }

    public static function unsignedOf(float $value): self
    {
        return new static($value, NumberStateImpl::Unsigned);
    }

    public static function signedOf(float $value): self
    {
        return new static($value, NumberStateImpl::Signed);
    }

    public static function of(float $value): self
    {
        return static::signedOf($value);
    }

    public static function ofArray(array $values): array
    {
        return array_map(fn($value) => Number::of($value), $values);
    }

    public function isPositive(): bool
    {
        return $this->value >= 0;
    }

    public function float(): float
    {
        return $this->value;
    }

    public function int(): int
    {
        return $this->value;
    }

    public function betweenOf(float $min, float $max): bool
    {
        return ($this->value >= $min && $this->value <= $max);
    }

    public function oppositeConvert(?callable $operation = null): self
    {
        $newValue = $operation ? $operation($this->value) : $this->value;

        return new self($newValue, $this->state->opposite());
    }

    #[Pure] public function isUnsigned(): bool
    {
        return $this->state->isUnsigned();
    }

    public function isSigned(): bool
    {
        return !$this->isUnsigned();
    }

    #[Pure] public function multiply(self $number): self
    {
        return new self($number->value * $this->value, $this->state);
    }

    public function equals(Number $expected): bool
    {
        return $this->value == $expected->value
            && $expected->state && $this->state;
    }

    #[Pure] public function division(Number $number): self
    {
        return new self($this->value / $number->value, $this->state);
    }

    public function than(callable $expression): bool
    {
        return $expression($this->value);
    }

    public function not(callable $expression): self
    {
        $value = $expression($this->value);
        return new self($value, $this->state);
    }

    public function percent(Number $percent): self
    {
        $percent = $percent->division(Number::of(100));
        return $this->multiply($percent);
    }

}