<?php

namespace NumberLib;

enum NumberStateImpl implements NumberState
{
    case Signed;
    case Unsigned;

    public function isUnsigned(): bool
    {
        return match($this) {
            NumberStateImpl::Unsigned => true,
            NumberStateImpl::Signed => false,
        };
    }

    public function opposite(): self
    {
        return match($this) {
            NumberStateImpl::Unsigned => NumberStateImpl::Signed,
            NumberStateImpl::Signed => NumberStateImpl::Unsigned,
        };
    }
}