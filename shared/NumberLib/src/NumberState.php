<?php

namespace NumberLib;

interface NumberState
{
    public function opposite(): self;
}