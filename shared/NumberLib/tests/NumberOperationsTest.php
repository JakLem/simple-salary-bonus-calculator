<?php

namespace NumberLibTests;

use NumberLib\Number;
use PHPUnit\Framework\TestCase;

class NumberOperationsTest extends TestCase
{
    public function test_should_change_number_if_higher_than()
    {
        $expected = Number::of(10);

        $number = Number::of(15);

        $number = $number->not(fn ($value) => min($value, 10));
        self::assertTrue($expected->equals($number));
    }

    public function test_should_return_properly_percentage_result()
    {
        $expected = Number::of(15);

        $number = Number::of(100);
        $percent = Number::of(15);

        $number = $number->percent($percent);
        self::assertTrue($expected->equals($number));
    }
}