<?php

namespace NumberLibTests;

use NumberLib\Number;
use PHPUnit\Framework\TestCase;

class NumberTest extends TestCase
{
    public function test_should_check_if_number_is_between_and_unsigned()
    {
        $number = Number::unsignedOf(5);

        self::assertTrue($number->betweenOf(3, 6) && $number->isUnsigned());
    }

    public function test_should_check_if_number_is_between_and_signed()
    {
        $number = Number::signedOf(5);

        self::assertTrue($number->betweenOf(3, 6) && !$number->isUnsigned());
    }

    public function test_should_return_division_number()
    {
        $number = Number::of(100);
        $divisionNumber = Number::of(2);

        $expectedNumber = Number::of(50);

        //when
        $result = $number->division($divisionNumber);

        //then
        self::assertTrue($result->equals($expectedNumber));
    }

    public function test_should_point_number_greater_than()
    {
        $number = Number::of(100);

        //when
        $result = $number->than(fn ($value) => $value > 10);

        self::assertTrue($result);
    }

    public function test_should_point_number_lower_equal_than()
    {
        $number = Number::of(10);

        //when
        $result = $number->than(fn ($value) => $value <= 10);

        self::assertTrue($result);
    }
}