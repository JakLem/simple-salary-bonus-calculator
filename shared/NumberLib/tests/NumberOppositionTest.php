<?php

namespace NumberLibTests;

use NumberLib\Number;
use PHPUnit\Framework\TestCase;

class NumberOppositionTest extends TestCase
{
    public function test_should_check_if_number_is_unsigned_after_convert()
    {
        //given
        $number = Number::signedOf(5);

        //when
        $number = $number->oppositeConvert(fn ($numberValue) => $numberValue);

        //then
        self::assertTrue($number->isUnsigned());
    }

    public function test_should_check_if_number_is_unsigned_after_default_convert()
    {
        //given
        $number = Number::signedOf(5);

        //when
        $number = $number->oppositeConvert();

        //then
        self::assertTrue($number->isUnsigned());
    }

    public function test_should_check_if_number_is_signed_after_convert()
    {
        //given
        $number = Number::unsignedOf(5);

        //when
        $number = $number->oppositeConvert(
            fn ($numberValue) => max($numberValue, 0)
        );

        //then
        self::assertTrue($number->isSigned());
    }

    public function test_should_check_if_number_is_positive_signed_after_convert()
    {
        //given
        $expectedResultAfterConvert = 0;
        $number = Number::unsignedOf(-4);

        //when
        $number = $number->oppositeConvert(
            fn ($numberValue) => max($numberValue, 0)
        );

        //then
        self::assertTrue($number->isSigned());
        self::assertEquals($expectedResultAfterConvert, $number->int());
    }
}