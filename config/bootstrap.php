<?php
// bootstrap.php
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;
$config = Setup::createAttributeMetadataConfiguration(
    array(__DIR__."/../src"),
    $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader
);
// or if you prefer yaml or XML
// $config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);
// $config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);

// database configuration parameters
$conn = array(
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . '/db.sqlite',
);

Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');

// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);
